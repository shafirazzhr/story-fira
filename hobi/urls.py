from django.urls import include, path


from . import views
from .views import hobi

app_name = 'hobi'

urlpatterns = [
   path('', views.hobi, name='hobi'),
]
